package model;

import java.awt.Color;
import java.awt.Point;

public class CaseModel {

    private final Point point;
    private final Color color;

    public CaseModel(final Point point, final Color color) {
        this.point = point;
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }

    @Override
    public String toString() {
        return "[" + this.point.getY() + "][" + this.point.getY() + "] color : " + this.color;
    }
}
