package model.tetrominos;

import main.Reglages;

public class J_Tetromino extends Tetromino {

    private static final long serialVersionUID = -766390512473188901L;

    private int shapeNumber;

    public J_Tetromino() {
        super.shape = new boolean[3][3];
        super.color = Reglages.COLORS[1];
        this.setShape();
    }

    @Override
    public void rotate() {
        for (var i = 0; i < this.shape.length; ++i) {
            for (var j = 0; j < this.shape[i].length; ++j) {
                this.shape[i][j] = false;
            }
        }
        this.shapeNumber %= 4;
        ++this.shapeNumber;

        this.setShape();
    }

    @Override
    public void antiRotate() {
        for (var i = 0; i < this.shape.length; ++i) {
            for (var j = 0; j < this.shape[i].length; ++j) {
                this.shape[i][j] = false;
            }
        }
        if (--this.shapeNumber == 0) {
            this.shapeNumber = 4;
        }
        this.setShape();
    }

    private void setShape() {
        switch (this.shapeNumber) {
            case 1:
                // shape = {
                //      {1, 0, 0},
                //      {1, 1, 1},
                //      {0, 0, 0}
                // };
                this.shape[0][0] = this.shape[1][0] = this.shape[1][1] = this.shape[1][2] = true;
                break;

            case 2:
                // shape = {
                //      {0, 1, 1},
                //      {0, 1, 0},
                //      {0, 1, 0}
                // };
                this.shape[0][2] = this.shape[0][1] = this.shape[1][1] = this.shape[2][1] = true;
                break;

            case 3:
                // shape = {
                //      {0, 0, 0},
                //      {1, 1, 1},
                //      {0, 0, 1}
                // };
                this.shape[1][0] = this.shape[1][1] = this.shape[1][2] = this.shape[2][2] = true;
                break;
            case 4:
                // shape = {
                //      {0, 1, 0},
                //      {0, 1, 0},
                //      {1, 1, 0}
                // };
                this.shape[2][0] = this.shape[2][1] = this.shape[1][1] = this.shape[0][1] = true;
                break;

            default:
                // shape = {
                //      {1, 0, 0},
                //      {1, 1, 1},
                //      {0, 0, 0}
                // };
                this.shape[0][0] = this.shape[1][0] = this.shape[1][1] = this.shape[1][2] = true;
                break;
        }
    }
}
