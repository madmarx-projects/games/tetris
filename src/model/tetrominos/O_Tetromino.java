package model.tetrominos;

import main.Reglages;

public class O_Tetromino extends Tetromino {

    private static final long serialVersionUID = 2792367081034455036L;

    private int shapeNumber;

    public O_Tetromino() {
        super.shape = new boolean[4][4];
        super.color = Reglages.COLORS[3];
        this.nextShape();
    }

    @Override
    public void rotate() {
        for (var i = 0; i < this.shape.length; ++i) {
            for (var j = 0; j < this.shape[i].length; ++j) {
                this.shape[i][j] = false;
            }
        }
        this.nextShape();
    }

    private void nextShape() {
        this.shapeNumber %= 4;
        ++this.shapeNumber;

        switch (this.shapeNumber) {
            default:
                // shape = {
                //      {0, 1, 1, 0},
                //      {0, 1, 1, 0},
                //      {0, 0, 0, 0},
                //      {0, 0, 0, 0}
                // };
                this.shape[0][1] = this.shape[0][2] = this.shape[1][1] = this.shape[1][2] = true;
                break;

        }
    }
}
