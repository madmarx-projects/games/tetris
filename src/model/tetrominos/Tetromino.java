package model.tetrominos;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JPanel;

public abstract class Tetromino extends JPanel {

    private static final long serialVersionUID = -612433905829799854L;

    protected boolean[][] shape;
    protected Color       color;
    protected Point       position = new Point(4, -2);

    public void fall() {
        this.position.setLocation(this.position.getX(), this.position.getY() + 1);
    }

    public void rotate() {}

    public void antiRotate() {}

    public void moveLeft() {
        this.position.setLocation(this.position.getX() - 1, this.position.getY());
    }

    public void moveRight() {
        this.position.setLocation(this.position.getX() + 1, this.position.getY());
    }

    public boolean[][] getShape() {
        return this.shape;
    }

    public Color getColor() {
        return this.color;
    }

    public Point getPosition() {
        return this.position;
    }

}
