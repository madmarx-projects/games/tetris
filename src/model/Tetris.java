package model;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.Timer;

import factory.TetrominoFactory;
import gui.utils.Chronometer;
import main.Reglages;
import model.tetrominos.Tetromino;
import observer.Observable;
import states.GameOverState;
import states.NotRunningState;
import states.State;

public class Tetris extends Observable {

    private int score;
    private int lines;
    private int level;
    private int speed;

    private CaseModel[][] board;

    private Tetromino currentTetromino;
    private Tetromino nextTetromino;

    private Timer       timerDescente;
    private State       state;
    private Chronometer chronometer;

    public Tetris() {
        //TODO nettoyer le jeu pour recommencer une partie
        this.init();
    }

    public void init() {
        this.board = new CaseModel[Reglages.GAME_HEIGHT][Reglages.GAME_WIDTH];
        this.currentTetromino = TetrominoFactory.getRandomTetromino();
        this.nextTetromino = TetrominoFactory.getRandomTetromino();

        this.score = this.lines = 0;
        this.level = 1;
        this.speed = Reglages.SPEED[this.level - 1];

        this.setState(NotRunningState.getInstance());
        this.chronometer = new Chronometer();
    }

    public void keyPressed(final KeyEvent e) {
        this.state.handle(this, e);
    }

    public void runGame() {
        this.resumeGame();
    }

    public void pauseGame() {
        this.pauseChrono();
        this.notifyAllObserversStatePause();
        this.stopDescente();
    }

    public void resumeGame() {
        this.startChrono();
        this.notifyAllObserversStateRunning();

        this.startDescente(this.speed);
    }

    public void startChrono() {
        this.chronometer.start(this);
        this.notifyAllObserversTimer();
    }

    public void pauseChrono() {
        this.chronometer.pause();
    }

    private void fall() {
        if (this.canFall()) {
            this.currentTetromino.fall();
        } else {
            // on dépose les couleurs du currentTetromino dans la grille
            final var shape = this.currentTetromino.getShape();
            int x, y;

            for (var i = 0; i < shape.length; ++i) {
                for (var j = 0; j < shape[i].length; ++j) {
                    if (shape[i][j]) {
                        y = i + (int) this.currentTetromino.getPosition().getY();
                        x = j + (int) this.currentTetromino.getPosition().getX();
                        try {
                            this.board[y][x] = new CaseModel(new Point(x, y), this.currentTetromino.getColor());
                        }
                        catch (final Exception e) {
                            // game over car quitte la grille de jeu
                            this.notifyAllObserversGameOver();
                            this.chronometer.stop();
                            this.stopDescente();
                            this.setState(GameOverState.getInstance());
                            return;
                        }

                    }
                }
            }

            final var linesCompleted = this.checkLines();
            if (linesCompleted >= 0) {
                this.score += Reglages.BONUS[linesCompleted];
                this.lines += linesCompleted;
                if (this.level < 15) {
                    this.level = (int) (Math.sqrt(this.score) / 4.5) + 1;
                }
            }
            this.speed = Reglages.SPEED[this.level - 1];

            this.stopDescente();
            this.startDescente(this.speed);

            this.currentTetromino = this.nextTetromino;
            this.nextTetromino = TetrominoFactory.getRandomTetromino();
        }

        this.notifyAllObservers();
    }

    private boolean canFall() {
        final var shape = this.currentTetromino.getShape();
        int x, y;

        for (var lig = shape.length - 1; lig >= 0; --lig) {
            for (var col = 0; col < shape[lig].length; ++col) {
                if (shape[lig][col]) {
                    y = lig + (int) this.currentTetromino.getPosition().getY();
                    x = col + (int) this.currentTetromino.getPosition().getX();
                    if ((x >= 0 && x < Reglages.GAME_WIDTH && y >= 0 && y < Reglages.GAME_HEIGHT - 1
                            && this.board[y + 1][x] != null) || y >= Reglages.GAME_HEIGHT - 1) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public void rotateCurrentTetromino() {
        this.currentTetromino.rotate();
        if (this.shouldAntiRotate()) {
            this.currentTetromino.antiRotate();
        }
        this.notifyAllObserversBoard();
    }

    private boolean shouldAntiRotate() {
        final var shape = this.currentTetromino.getShape();
        int x, y;

        for (var lig = 0; lig < shape.length; ++lig) {
            for (var col = 0; col < shape[lig].length; ++col) {
                if (shape[lig][col]) {
                    y = lig + (int) this.currentTetromino.getPosition().getY(); // y = position dans la board
                    x = col + (int) this.currentTetromino.getPosition().getX();
                    if (x < 0 || x >= Reglages.GAME_WIDTH || y < -2 && y >= Reglages.GAME_HEIGHT
                            || y >= 0 && this.board[y][x] != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void moveLeftCurrentTetromino() {
        if (this.canMoveLeft()) {
            this.currentTetromino.moveLeft();
            this.notifyAllObserversBoard();
        }
    }

    private boolean canMoveLeft() {
        final var shape = this.currentTetromino.getShape();
        int x, y;

        for (var col = 0; col < shape.length; ++col) {
            for (var lig = shape[col].length - 1; lig >= 0; --lig) {
                if (shape[lig][col]) {
                    y = lig + (int) this.currentTetromino.getPosition().getY();
                    x = col + (int) this.currentTetromino.getPosition().getX();

                    try {
                        if ((y >= 0 && x >= 0 && x < Reglages.GAME_WIDTH && this.board[y][x - 1] != null)
                                || x > Reglages.GAME_WIDTH) {
                            return false;
                        }

                    }
                    catch (final ArrayIndexOutOfBoundsException e) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public void moveRightCurrentTetromino() {
        if (this.canMoveRight()) {
            this.currentTetromino.moveRight();
            this.notifyAllObserversBoard();
        }
    }

    private boolean canMoveRight() {
        final var shape = this.currentTetromino.getShape();
        int x, y;

        for (var col = shape.length - 1; col >= 0; --col) {
            for (var lig = shape[0].length - 1; lig >= 0; --lig) {
                if (shape[lig][col]) {
                    y = lig + (int) this.currentTetromino.getPosition().getY();
                    x = col + (int) this.currentTetromino.getPosition().getX();

                    try {
                        if (x + 1 >= Reglages.GAME_WIDTH
                                || (x >= 0 && x < Reglages.GAME_WIDTH - 1 && y >= 0 && this.board[y][x + 1] != null)
                                || col + x > Reglages.GAME_WIDTH) {
                            return false;
                        }
                    }
                    catch (final ArrayIndexOutOfBoundsException e) {

                    }
                }
            }
        }

        return true;
    }

    private int checkLines() {
        final var shape = this.currentTetromino.getShape();
        final var y = (int) this.currentTetromino.getPosition().getY();
        final var x = (int) this.currentTetromino.getPosition().getX();

        var cptlines = 0;
        for (var lig = y; lig < y + shape[0].length; ++lig) {
            if (lig >= 0 && lig < Reglages.GAME_HEIGHT && this.lineFull(lig)) {
                this.removeLine(lig);
                ++cptlines;
            }
        }
        return cptlines;
    }

    private boolean lineFull(final int line) {
        for (var col = 0; col < Reglages.GAME_WIDTH; ++col) {
            if (this.board[line][col] == null) { return false; }
        }
        return true;
    }

    private void removeLine(final int line) {
        for (var col = 0; col < Reglages.GAME_WIDTH; ++col) {
            for (var lig = line; lig >= 0; --lig) {
                if (lig > 0) {
                    this.board[lig][col] = this.board[lig - 1][col];
                } else {
                    this.board[lig][col] = null;
                }
            }
        }
    }

    public void accelerateCurrentTetromino() {
        this.stopDescente();
        this.startDescente(Reglages.SPEED[15 - 1]);
    }

    private void startDescente(final int period) {
        this.timerDescente = new Timer("DESCENTE", true);
        this.timerDescente.schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                Tetris.this.fall();
                //board[-1][-1].getColor();  // faire planter le thread
            }
        }, 0, period);
    }

    private void stopDescente() {
        this.timerDescente.cancel();
    }

    public void setState(final State state) {
        System.out.println("[" + this.getClass().getName() + "] new state : " + state);
        this.state = state;
    }

    public CaseModel[][] getBoard() {
        return this.board;
    }

    public Tetromino getCurrentTetromino() {
        return this.currentTetromino;
    }

    public Tetromino getNextTetromino() {
        return this.nextTetromino;
    }

    public String getTime() {
        return this.chronometer.toString();
    }

    public int getScore() {
        return this.score;
    }

    public int getLines() {
        return this.lines;
    }

    public int getLevel() {
        return this.level;
    }
}
