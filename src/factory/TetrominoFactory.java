package factory;

import model.tetrominos.I_Tetromino;
import model.tetrominos.J_Tetromino;
import model.tetrominos.L_Tetromino;
import model.tetrominos.O_Tetromino;
import model.tetrominos.S_Tetromino;
import model.tetrominos.T_Tetromino;
import model.tetrominos.Tetromino;
import model.tetrominos.Z_Tetromino;

public final class TetrominoFactory {

    public static Tetromino getRandomTetromino() {
        switch ((int) (Math.random() * 7)) {
            case 0:
                return new I_Tetromino();

            case 1:
                return new J_Tetromino();

            case 2:
                return new L_Tetromino();

            case 3:
                return new O_Tetromino();

            case 4:
                return new S_Tetromino();

            case 5:
                return new T_Tetromino();

            case 6:
                return new Z_Tetromino();

            default:
                return new O_Tetromino();
        }
    }

}
