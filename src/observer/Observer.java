package observer;

public interface Observer {
    void updateAll();

    void updateBoard();

    void updateScore();

    void updateLines();

    void updateLevel();

    void updateTimer();

    void updateStateToRunning();

    void updateStateToPause();

    void gameOver();
}
