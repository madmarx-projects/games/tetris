package observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {

    private final List<Observer> observers = new ArrayList<>();

    public void attach(final Observer observer) {
        this.observers.add(observer);
    }

    protected void notifyAllObservers() {
        for (final Observer observer : this.observers) {
            observer.updateAll();
        }
    }

    protected void notifyAllObserversBoard() {
        for (final Observer observer : this.observers) {
            observer.updateBoard();
        }
    }

    protected void notifyAllObserversScore() {
        for (final Observer observer : this.observers) {
            observer.updateScore();
        }
    }

    public void notifyAllObserversTimer() {
        for (final Observer observer : this.observers) {
            observer.updateTimer();
        }
    }

    protected void notifyAllObserversStateRunning() {
        for (final Observer observer : this.observers) {
            observer.updateStateToRunning();
        }
    }

    protected void notifyAllObserversStatePause() {
        for (final Observer observer : this.observers) {
            observer.updateStateToPause();
        }
    }

    protected void notifyAllObserversGameOver() {
        for (final Observer observer : this.observers) {
            observer.gameOver();
        }
    }

}
