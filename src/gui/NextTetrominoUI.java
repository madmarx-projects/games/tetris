package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JPanel;

import gui.utils.Case;
import gui.utils.InfoPanelUI;
import model.tetrominos.Tetromino;

// Tetromino affiché dans le coin suppérieur droit
public class NextTetrominoUI extends InfoPanelUI {

    private static final long serialVersionUID = 2558483215246122534L;

    private static final int DIM_CONTAINER = 4;
    private static final int GAP           = 1;

    private final JPanel             pnl_tetromino_container;
    private final Case               cases[][];
    private final GridBagConstraints gbc;

    public NextTetrominoUI() {
        super("NEXT", new Point(298, 48), new Dimension(94, 76));

        // ÉLÉMENTS
        this.gbc = new GridBagConstraints();
        this.gbc.gridy = 1;
        this.gbc.weighty = 1;
        this.gbc.anchor = GridBagConstraints.PAGE_START;

        this.pnl_tetromino_container = new JPanel();
        this.pnl_tetromino_container.setLayout(new GridLayout(DIM_CONTAINER, DIM_CONTAINER, GAP, GAP));
        this.pnl_tetromino_container.setOpaque(false);

        // pré-remplir le grille
        this.cases = new Case[DIM_CONTAINER][DIM_CONTAINER];
        for (var i = 0; i < DIM_CONTAINER; ++i) {
            for (var j = 0; j < DIM_CONTAINER; ++j) {
                this.cases[i][j] = new Case();
                this.pnl_tetromino_container.add(this.cases[i][j]);
            }
        }

        this.add(this.pnl_tetromino_container, this.gbc);

    }

    public void updateTetromino(final Tetromino tetromino) {
        final var shape = tetromino.getShape();

        for (var i = 0; i < DIM_CONTAINER; ++i) {
            for (var j = 0; j < DIM_CONTAINER; ++j) {
                try {
                    this.cases[i][j].setOpaque(false, Color.LIGHT_GRAY);
                    if (shape[i][j]) {
                        this.cases[i][j].changeColor(tetromino.getColor());
                    }
                }
                catch (final IndexOutOfBoundsException e) {

                }
            }
        }
    }

}
