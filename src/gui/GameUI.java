package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import main.Reglages;
import model.Tetris;
import observer.Observer;

public class GameUI implements Observer {

    private final JFrame    frame;
    private TetrisUI        tetrisUI;
    private NextTetrominoUI nextTetrominoUI;
    private StateUI         stateUI;
    private ScoreUI         scoreUI;
    private LinesUI         linesUI;
    private LevelUI         levelUI;
    private TimerUI         timerUI;

    private Tetris tetris;

    public GameUI() {
        this.frame = new JFrame("Tetris 1337");
        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.setResizable(false);

        try {
            System.out.println("[" + this.getClass().getName() + "] Trying to load background image at "
                    + Reglages.BACKGROUND_PATH);

            final var img = ImageIO.read(new File(Reglages.BACKGROUND_PATH));

            System.out.println("[" + this.getClass().getName() + "] Game ready");

            this.frame.setContentPane(new JLabel(new ImageIcon(img)));
            this.frame.setLayout(null);

            this.tetrisUI = new TetrisUI();
            this.nextTetrominoUI = new NextTetrominoUI();
            this.stateUI = new StateUI();
            this.scoreUI = new ScoreUI();
            this.linesUI = new LinesUI();
            this.levelUI = new LevelUI();
            this.timerUI = new TimerUI();

            this.frame.add(this.tetrisUI);
            this.frame.add(this.nextTetrominoUI);
            this.frame.add(this.stateUI);
            this.frame.add(this.scoreUI);
            this.frame.add(this.linesUI);
            this.frame.add(this.levelUI);
            this.frame.add(this.timerUI);

            this.frame.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(final KeyEvent e) {
                    GameUI.this.tetris.keyPressed(e);
                }
            });

            this.tetris = new Tetris();
            this.tetris.attach(this);

        }
        catch (final IOException e) {
            System.err.println("[GameUI] Échec");
            e.printStackTrace();

            this.frame.setLayout(new GridBagLayout());
            final var gbc = new GridBagConstraints();
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            this.frame.setPreferredSize(new Dimension(500, 500));

            this.frame.add(new JLabel("Le jeu ne peut pas démarrer sans image de fond"), gbc);
            this.frame.add(new JLabel("Supposé ici : " + Reglages.BACKGROUND_PATH), gbc);
        }

        this.frame.pack();
        this.frame.setLocationRelativeTo(null); // la fenêtre se centre au milieu de l'écran
        this.frame.setVisible(true);
    }

    //IMPLEMENTS

    @Override
    public void updateAll() {
        this.updateBoard();
        this.updateNextTetromino();
        this.updateScore();
        this.updateLines();
        this.updateLevel();
    }

    @Override
    public void updateBoard() {
        this.tetrisUI.updateAll(this.tetris);
        this.updateCurrentTetromino();
    }

    public void updateCurrentTetromino() {
        this.tetrisUI.updateTetromino(this.tetris.getCurrentTetromino());
    }

    public void updateNextTetromino() {
        this.nextTetrominoUI.updateTetromino(this.tetris.getNextTetromino());
    }

    @Override
    public void updateScore() {
        this.scoreUI.updateScoreUI(this.tetris.getScore());
    }

    @Override
    public void updateLines() {
        this.linesUI.updateLinesUI(this.tetris.getLines());
    }

    @Override
    public void updateLevel() {
        this.levelUI.updateLevelUI(this.tetris.getLevel());
    }

    @Override
    public void updateTimer() {
        this.timerUI.updateTime(this.tetris.getTime());
    }

    @Override
    public void updateStateToRunning() {
        this.stateUI.stateChangedToRunning();
    }

    @Override
    public void updateStateToPause() {
        this.stateUI.stateChangedToPause();
    }

    @Override
    public void gameOver() {
        this.stateUI.stateChangedToGameOver();
    }

    public TetrisUI getTetrisUI() {
        return this.tetrisUI;
    }

    public NextTetrominoUI getNextTetrominoUI() {
        return this.nextTetrominoUI;
    }

    public StateUI getStateUI() {
        return this.stateUI;
    }

    public ScoreUI getScoreUI() {
        return this.scoreUI;
    }

    public TimerUI getTimerUI() {
        return this.timerUI;
    }

}
