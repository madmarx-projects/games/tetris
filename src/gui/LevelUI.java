package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Point;

import javax.swing.JLabel;

import gui.utils.InfoPanelUI;

public class LevelUI extends InfoPanelUI {

    private static final long serialVersionUID = 3178360203079390193L;

    private final JLabel lb_level;

    public LevelUI() {
        super("LEVEL", new Point(8, 191), new Dimension(93, 38));

        // ÉLÉMENTS
        final var gbc = new GridBagConstraints();
        gbc.gridy = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.PAGE_START;

        this.lb_level = new JLabel("--");
        this.lb_level.setFont(new Font(super.getFont().getFontName(), Font.PLAIN, 15));
        this.lb_level.setForeground(Color.white);

        this.add(this.lb_level, gbc);
    }

    public void updateLevelUI(final int level) {
        this.lb_level.setText(String.valueOf(level));
    }

}
