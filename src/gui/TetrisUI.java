package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JPanel;

import gui.utils.Case;
import main.Reglages;
import model.Tetris;
import model.tetrominos.Tetromino;

// Puits affiché au centre (aire de jeu)
public class TetrisUI extends JPanel {

    private static final long serialVersionUID = -2216208710191137757L;

    private final Case[][] cases;

    public TetrisUI() {
        final var position = new Point(116, 28);
        final var dimension = new Dimension(170, 372);

        this.setLayout(new GridLayout(Reglages.GAME_HEIGHT, Reglages.GAME_WIDTH, 2, 2));
        this.setOpaque(false);
        this.setPreferredSize(dimension);
        this.setBounds(new Rectangle(position, dimension));

        // pré-remplir le grille
        this.cases = new Case[Reglages.GAME_HEIGHT][Reglages.GAME_WIDTH];
        for (var i = 0; i < Reglages.GAME_HEIGHT; ++i) {
            for (var j = 0; j < Reglages.GAME_WIDTH; ++j) {
                this.cases[i][j] = new Case();
                this.add(this.cases[i][j]);
            }
        }
    }

    public void updateTetromino(final Tetromino tetromino) {

        final var shape = tetromino.getShape();
        final var color = tetromino.getColor();
        final var position = tetromino.getPosition();

        for (var i = 0; i < shape.length; ++i) {
            for (var j = 0; j < shape[i].length; ++j) {
                if (shape[i][j]) {
                    try {
                        this.cases[i + (int) position.getY()][j + (int) position.getX()].changeColor(color);
                    }
                    catch (final ArrayIndexOutOfBoundsException e) {

                    }
                }
            }
        }
    }

    public void updateAll(final Tetris tetris) {
        for (var i = 0; i < tetris.getBoard().length; ++i) {
            for (var j = 0; j < tetris.getBoard()[i].length; ++j) {
                if (tetris.getBoard()[i][j] != null) {
                    this.cases[i][j].changeColor(tetris.getBoard()[i][j].getColor());
                } else {
                    this.cases[i][j].setOpaque(false, Color.DARK_GRAY);
                }
            }
        }
    }

    public Case[][] getCases() {
        return this.cases;
    }

}
