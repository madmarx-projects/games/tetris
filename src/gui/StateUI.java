package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Point;

import javax.swing.JLabel;

import gui.utils.InfoPanelUI;

// État affiché dans le coin inférieur gauche
public class StateUI extends InfoPanelUI {

    private static final long serialVersionUID = -8430520196606902498L;

    private final JLabel lb_state;
    private final JLabel lb_state_suite;

    public StateUI() {
        super("ÉTAT", new Point(8, 320), new Dimension(94, 93));

        // ÉLÉMENTS
        final var gbc = new GridBagConstraints();

        this.lb_state = new JLabel("Press 'space'");
        this.lb_state.setFont(new Font(super.getFont().getFontName(), Font.PLAIN, 10));
        this.lb_state.setForeground(Color.white);
        gbc.gridy = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(this.lb_state, gbc);

        this.lb_state_suite = new JLabel("pour commencer");
        this.lb_state_suite.setFont(new Font(super.getFont().getFontName(), Font.PLAIN, 9));
        this.lb_state_suite.setForeground(Color.white);
        gbc.gridy = 2;
        gbc.weighty = 1;
        this.add(this.lb_state_suite, gbc);
    }

    public void stateChangedToRunning() {
        this.lb_state.setText("Press 'p'");
        this.lb_state_suite.setText("to pause");
    }

    public void stateChangedToPause() {
        this.lb_state.setText("Press 'p'");
        this.lb_state_suite.setText("to resume");
    }

    public void stateChangedToGameOver() {
        this.lb_state.setFont(new Font(super.getFont().getFontName(), Font.PLAIN, 14));
        this.lb_state.setForeground(Color.RED);
        this.lb_state.setText("GAMEOVER");
        this.lb_state_suite.setText("");
    }

}
