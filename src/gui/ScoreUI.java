package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Point;

import javax.swing.JLabel;

import gui.utils.InfoPanelUI;

// Score affiché dans le coin suppérieur gauche
public class ScoreUI extends InfoPanelUI {

    private static final long serialVersionUID = -744934130530956161L;

    private final JLabel lb_score;

    public ScoreUI() {
        super("SCORE", new Point(8, 48), new Dimension(93, 76));

        // ÉLÉMENTS
        final var gbc = new GridBagConstraints();
        gbc.gridy = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.PAGE_START;

        this.lb_score = new JLabel("--");
        this.lb_score.setFont(new Font(super.getFont().getFontName(), Font.PLAIN, 19));
        this.lb_score.setForeground(Color.white);

        this.add(this.lb_score, gbc);
    }

    public void updateScoreUI(final int score) {
        this.lb_score.setText(String.valueOf(score));
    }

}
