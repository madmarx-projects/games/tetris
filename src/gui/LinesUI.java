package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Point;

import javax.swing.JLabel;

import gui.utils.InfoPanelUI;

public class LinesUI extends InfoPanelUI {

    private static final long serialVersionUID = 5426470352399932494L;

    private final JLabel lb_lines;

    public LinesUI() {
        super("LIGNES", new Point(8, 138), new Dimension(93, 38));

        // ÉLÉMENTS
        final var gbc = new GridBagConstraints();
        gbc.gridy = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.PAGE_START;

        this.lb_lines = new JLabel("--");
        this.lb_lines.setFont(new Font(super.getFont().getFontName(), Font.PLAIN, 15));
        this.lb_lines.setForeground(Color.white);

        this.add(this.lb_lines, gbc);
    }

    public void updateLinesUI(final int lines) {
        this.lb_lines.setText(String.valueOf(lines));
    }

}
