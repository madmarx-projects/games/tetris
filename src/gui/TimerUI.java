package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Point;

import javax.swing.JLabel;

import gui.utils.InfoPanelUI;

// Timer affiché dans le coin inférieur droit
public class TimerUI extends InfoPanelUI {

    private static final long serialVersionUID = 3546972518407011382L;

    private final JLabel lb_timer;

    public TimerUI() {
        super("TIMER", new Point(298, 320), new Dimension(93, 42));

        // ÉLÉMENTS
        final var gbc = new GridBagConstraints();
        gbc.gridy = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.PAGE_START;

        this.lb_timer = new JLabel("00:00");
        this.lb_timer.setFont(new Font(super.getFont().getFontName(), Font.PLAIN, 15));
        this.lb_timer.setForeground(Color.white);

        super.add(this.lb_timer, gbc);
    }

    public void updateTime(final String updatedTime) {
        this.lb_timer.setText(updatedTime);
    }

}
