package gui.utils;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

import main.Reglages;

public class Case extends JPanel {

    private static final long serialVersionUID = 6526899099653760077L;

    public Case() {
        this.setPreferredSize(new Dimension(Reglages.CASE_LARG, Reglages.CASE_HAUT));
        this.setOpaque(false, Color.DARK_GRAY);
    }

    public void changeColor(final Color color) {
        this.setBackground(color);
        //setBorder(new LineBorder(Color.GRAY, 1));
        this.setOpaque(true);
    }

    public void setOpaque(final boolean isOpaque, final Color color) {
        this.setBackground(color);
        this.setOpaque(isOpaque);
    }

}
