package gui.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;

public class InfoPanelTitleUI extends JLabel {

    private static final long serialVersionUID = -5577862698474504814L;

    private GridBagConstraints gbc_title;

    public InfoPanelTitleUI(final String title) {
        super(title);
        this.setFont(new Font("Dialog.bold", Font.PLAIN, 10));
        this.setForeground(Color.cyan);

        this.gbc_title = new GridBagConstraints();
        this.gbc_title.gridwidth = GridBagConstraints.REMAINDER;
        this.gbc_title.weighty = 1;
        this.gbc_title.anchor = GridBagConstraints.PAGE_START;
        this.gbc_title.gridy = 0;
        this.gbc_title.insets = new Insets(2, 0, 3, 0);
    }

    public GridBagConstraints getConstraints() {
        return this.gbc_title;
    }

    public void setConstraints(final GridBagConstraints gbc_title) {
        this.gbc_title = gbc_title;
    }

}
