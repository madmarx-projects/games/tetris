package gui.utils;

import java.util.Timer;

import model.Tetris;

public class Chronometer {

    private int   m;
    private int   s;
    private Timer timer;

    public Chronometer() {
        this.m = 0;
        this.s = 0;
    }

    public void start(final Tetris tetris) {
        this.timer = new Timer("CHRONO", true);
        this.timer.schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                Chronometer.this.tick();
                tetris.notifyAllObserversTimer();
            }
        }, 1000, 1000);
    }

    public void pause() {
        try {
            this.timer.cancel();
        }
        catch (final IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void reset() {
        this.m = 0;
        this.s = 0;
    }

    private void tick() {
        ++this.s;
        this.m = this.m + this.s / 60;
        this.s %= 60;
    }

    @Override
    public String toString() {
        var time = "";
        time += ((this.m < 10) ? "0" : "") + this.m + ":";
        time += ((this.s < 10) ? "0" : "") + this.s;

        return time;
    }

    public void stop() {
        this.pause();
    }
}
