package gui.utils;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JPanel;

public abstract class InfoPanelUI extends JPanel {

    private static final long serialVersionUID = -4250899673806605118L;

    public InfoPanelUI(final String title, final Point position, final Dimension dimension) {
        final var titleUI = new InfoPanelTitleUI(title);

        this.setLayout(new GridBagLayout());
        this.setPreferredSize(dimension);
        this.setBounds(new Rectangle(position, dimension));
        this.setOpaque(false);
        this.setFont(new Font("Dialog.bold", Font.PLAIN, 10));

        this.add(titleUI, titleUI.getConstraints());
    }

}
