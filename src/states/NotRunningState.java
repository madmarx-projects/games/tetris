package states;

import java.awt.event.KeyEvent;

import main.Reglages;
import model.Tetris;

public class NotRunningState implements State {

    private static NotRunningState instance = new NotRunningState();
    private Tetris                 context;

    private NotRunningState() {}

    public static NotRunningState getInstance() {
        return instance;
    }

    @Override
    public void handle(final Tetris context,
            final KeyEvent event) {/*
                                    * System.out.println("[" +
                                    * getClass().getName() + "]\n" +
                                    * " * État quand la partie n'a pas encore démarré\n"
                                    * +
                                    * " * Label signalant la nécessité de presser la barre d'espace pour démarrer la partie\n"
                                    * + " * Timer figé\n" +
                                    * " * Tetriminos suivant déjà affiché dans la case en haut à droite"
                                    * );
                                    */
        // TODO remove me

        // changement d'état vers RunningState

        if (Character.toLowerCase(event.getKeyChar()) == Reglages.KEYS.PLAY.getValue()) {
            context.setState(RunningState.getInstance()); // état suivant
            context.runGame();
        }
    }

    @Override
    public String toString() {
        return "NotRunningState";
    }
}
