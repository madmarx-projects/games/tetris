package states;

import java.awt.event.KeyEvent;

import main.Reglages;
import model.Tetris;

public class PauseState implements State {

    private static PauseState ourInstance = new PauseState();
    private Tetris            context;

    private PauseState() {

    }

    public static PauseState getInstance() {
        return ourInstance;
    }

    @Override
    public void handle(final Tetris context,
            final KeyEvent event) {/*
                                    * System.out.println("[" +
                                    * getClass().getName() + "]\n" +
                                    * " * Timer qui s'arrête\n" +
                                    * " * Score qui ne s'incrémente plus\n" +
                                    * " * Tetriminos qui se figent\n" +
                                    * " * Label signalant la pause (en plein milieu de l'écran en grand?)"
                                    * );
                                    */
        // TODO remove me

        if (Character.toLowerCase(event.getKeyChar()) == Reglages.KEYS.PAUSE_RESUME.getValue()) {
            context.setState(RunningState.getInstance());
            context.resumeGame();
        }
    }

    @Override
    public String toString() {
        return "PauseState";
    }
}
