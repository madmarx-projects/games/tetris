package states;

import java.awt.event.KeyEvent;

import main.Reglages;
import model.Tetris;

public class GameOverState implements State {

    private static GameOverState instance = new GameOverState();
    private Tetris               context;

    private GameOverState() {}

    public static GameOverState getInstance() {
        return instance;
    }

    @Override
    public void handle(final Tetris context,
            final KeyEvent event) {/*
                                    * System.out.println("[" +
                                    * getClass().getName() + "]\n" +
                                    * " * État quand la partie n'a pas encore démarré\n"
                                    * +
                                    * " * Label signalant la nécessité de presser la barre d'espace pour démarrer la partie\n"
                                    * + " * Timer figé\n" +
                                    * " * Tetriminos suivant déjà affiché dans la case en haut à droite"
                                    * );
                                    */
        // TODO remove me

        // changement d'état vers RunningState
        if (Character.toLowerCase(event.getKeyChar()) == Reglages.KEYS.RESTART.getValue()) {
            //context.setState(NotRunningState.getInstance());    // TODO état suivant?
            //context.runGame();
            System.err.println(
                    "[" + this.getClass().getName() + "] " + "recommencer la game --> impossible pour l'instant");
        }
    }

    @Override
    public String toString() {
        return "GameOverState";
    }

}
