package states;

import java.awt.event.KeyEvent;

import main.Reglages;
import model.Tetris;

public class RunningState implements State {

    private static RunningState ourInstance = new RunningState();
    private Tetris              context;

    private RunningState() {}

    public static RunningState getInstance() {
        return ourInstance;
    }

    @Override
    public void handle(final Tetris context, final KeyEvent event) {
        final var key = Character.toLowerCase(event.getKeyChar());

        if (key == Reglages.KEYS.PAUSE_RESUME.getValue()) {
            context.setState(PauseState.getInstance());
            context.pauseGame();
        } else if (key == Reglages.KEYS.ROTATE.getValue()) {
            context.rotateCurrentTetromino();
        } else if (key == Reglages.KEYS.MOVE_LEFT.getValue()) {
            context.moveLeftCurrentTetromino();
        } else if (key == Reglages.KEYS.MOVE_RIGHT.getValue()) {
            context.moveRightCurrentTetromino();
        } else if (key == Reglages.KEYS.ACCELERATE.getValue()) {
            context.accelerateCurrentTetromino();
        }
    }

    @Override
    public String toString() {
        return "RunningState";
    }
}
