package states;

import java.awt.event.KeyEvent;

import model.Tetris;

public interface State {

    void handle(Tetris tetris, KeyEvent event);
}
