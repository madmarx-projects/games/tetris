package main;

import java.awt.Color;

public final class Reglages {

    public static final String BACKGROUND_PATH = "src/res/tetris_background.png";

    public static final int GAME_HEIGHT = 22;
    public static final int GAME_WIDTH  = 10;

    public static final int CASE_LARG   = 15; // largeur d'une case de tetromino
    public static final int CASE_HAUT   = 15; // hauteur d'une case de tetromino
    public static final int CASE_BORDER = 1;  // largeur de la bordure d'une case

    public static final Color[] COLORS       = {
            Color.green, Color.blue, Color.yellow, Color.red, Color.pink, Color.white, Color.orange
    };
    public static final Color   BORDER_COLOR = Color.CYAN;

    // 15 niveaux de vitesse
    public static final int[] SPEED = {
            500, 470, 440, 410, 380, 350, 320, 290, 250, 225, 200, 150, 125, 100, 90, 80
    };

    // bonus suivant le nombre de lignes complétées en un coup
    public static final int[] BONUS = {
            0, 40, 100, 300, 1200
    };

    public enum KEYS {
        PLAY(' '), //
        PAUSE_RESUME('p'), //
        MOVE_LEFT('q'), //
        MOVE_RIGHT('d'), //
        ROTATE('z'), //
        ACCELERATE('s'), //
        RESTART('r'); //

        private final char value;

        KEYS(final char value) {
            this.value = value;
        }

        public char getValue() {
            return this.value;
        }
    }
}
